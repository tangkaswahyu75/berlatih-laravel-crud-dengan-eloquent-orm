<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    const CREATED_AT = 'tanggal_dibuat';
    const UPDATED_AT = 'tanggal_diperbaharui';
    protected $table = "pertanyaan";
    protected $fillable = ["judul", "isi"];
    public $timestamps = true;
}
