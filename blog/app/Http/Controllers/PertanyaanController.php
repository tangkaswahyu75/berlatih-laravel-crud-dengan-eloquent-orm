<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Pertanyaan;

class PertanyaanController extends Controller
{
    public function create()
    {
        return view('pertanyaan.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required'
        ]);

        // $query = DB::table('pertanyaan')->insert([
        //     "judul" => $request["judul"],
        //     "isi" => $request["isi"]
        // ]);

        // $pertanyaan = new Pertanyaan;
        // $pertanyaan->judul = $request["judul"];
        // $pertanyaan->isi = $request["isi"];
        // $pertanyaan->save();

        $pertanyaan = Pertanyaan::create([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
        ]);

        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Disimpan');
    }

    public function index()
    {
        // $pertanyaan = DB::table('pertanyaan')->get();
        // return view('pertanyaan.index', compact('pertanyaan'));

        $pertanyaan = Pertanyaan::all();
        return view('pertanyaan.index', ['pertanyaan' => $pertanyaan]);
    }

    public function show($id)
    {
        // $pertanyaan =  DB::table('pertanyaan')->where('id', $id)->first();
        // return view('pertanyaan.show', compact('pertanyaan'));
        $pertanyaan = Pertanyaan::find($id);
        return view('pertanyaan.show', ['pertanyaan' => $pertanyaan]);
    }

    public function edit($id)
    {
        // $pertanyaan =  DB::table('pertanyaan')->where('id', $id)->first();
        // return view('pertanyaan.edit', compact('pertanyaan'));

        $pertanyaan = Pertanyaan::where('id', $id)->first();
        return view('pertanyaan.edit', ['pertanyaan' => $pertanyaan]);
    }

    public function update($id, Request $request)
    {
        // $query =  DB::table('pertanyaan')->where('id', $id)->update([
        //     'judul' => $request['judul'],
        //     'isi' => $request['isi']
        // ]);

        $request->validate([
            'judul' => ['required'],
            'isi' => ['required'],
        ]);
        Pertanyaan::where('id', $id)->update([
            "judul" => $request["judul"],
            "isi" => $request["isi"],
        ]);

        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Diupdate');
    }

    public function destroy($id)
    {
        // $query =  DB::table('pertanyaan')->where('id', $id)->delete();
        // return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Dihapus');
        Pertanyaan::destroy($id);
        return redirect('/pertanyaan');
    }
}
